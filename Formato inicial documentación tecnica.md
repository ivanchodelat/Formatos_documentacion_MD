# Documentación técnica requerimiento: 


|ID Matriz FRICE||
|:----|:--:|
|Proyecto ||
|Estado |[]Abierto []Pruebas [X]Aceptado []Rechazado |
|Fecha ||
|Observaciones ||

**Control de versiones del documento**

|[x]|Versión|Comentario|Desarrollador|Fecha|
|:-:|:-----:|:---------|:-----------:|:---:|
|[]|1.0|Versión Inicial, Entrega a pruebas consultor|
|[]|2.0|Versión Final, Cierre de Desarrollo
## 1.Objetos de Desarrollo y Diccionario

### Orden de trasporte

|Transporte|Usuario|Descripción|
|--|--|--|
|  |  |  |

### Código de la Transacción
|Transacción|Programa|Descripción|
|--|--|--|
|  |  |  |

### Paquete:


|Paquete|Grupo de funciones|
|:------|----|
|||

### OBJETOS:
>Se deben describir todos los objetos usados, tablas Z, Funciones Z y estandar, clases, badi’s, user-exits, cluster vistas. En caso de ser formato, se debe indicar estilos, formatos de edición, Fuentes para código de barras y logos usados. 

## 2. Diseño General de la Solución.

|Método de ejecución: [X]|
|--|
|[X] Online (enhancement)|
|[] Transaction|
|[] Batch (background process no periodic)|

>Breve descripción de lo que hace el desarrollo en palabras del negocio y por qué fue creado.

## 3.	Prerequisitos para ejecución del desarrollo.

>Prerrequisitos a tener en cuenta para poder ejecutar el desarrollo. Parámetros Constantes y Rangos ZBC_CONF/ZBC_UDC/ZBC0001, y para que se usa en el programa.
Descripción Set de datos usados. En caso de tener comandos externos del sistema operativo (sm69), indicarlos en esta sección> 

## 4.	Pasos de ejecución del programa.
> Paso a paso. Pantallas.


## 5.	Aspectos Seguridad.
>se debe documentar los objetos de autorización creados para el desarrollo, y en caso de hacer validaciones de acceso con archivos planos o tablas Z>

## 6.  Plantillas / Archivos / Anexos.
>En caso del programa de ser un cargue de datos que funcione con un archivo de datos de entrada, se debe incluir en esta.

## 7.	Revisión Código Code Inspector.
>Se deben cumplir todos los indicadores de la variante de inspección creada por MQA
 

## 8.	Observaciones/Aprobación Lider Abap del Proyecto

|No.|Observación|
|---|-----------|
|---|-----------|


|________________|__________________|________________|
|:-:|:-:|:-:|
|Coonsultor Abap|Líder Abap Proyecto|Gerente Proyecto|
| Iván Castillo D.|||

